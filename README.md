# Papers I Shall Read

- [Reverse Engineering Intel Last-Level Cache Complex Addressing Using Performance Counters](./raid15_maurice.pdf)
  - Abstract: Cache attacks, which exploit differences in timing to perform
        covert or side channels, are now well understood. Recent works leverage
        the last level cache to perform cache attacks across cores. This cache is
        split in slices, with one slice per core. While predicting the slices used by
        an address is simple in older processors, recent processors are using an
        undocumented technique called complex addressing. This renders some
        attacks more difficult and makes other attacks impossible, because of the
        loss of precision in the prediction of cache collisions.
        In this paper, we build an automatic and generic method for reverse en-
        gineering Intel’s last-level cache complex addressing, consequently ren-
        dering the class of cache attacks highly practical. Our method relies on
        CPU hardware performance counters to determine the cache slice an
        address is mapped to. We show that our method gives a more precise
        description of the complex addressing function than previous work. We
        validated our method by reversing the complex addressing functions on
        a diverse set of Intel processors. This set encompasses Sandy Bridge, Ivy
        Bridge and Haswell micro-architectures, with different number of cores,
        for mobile and server ranges of processors. We show the correctness of
        our function by building a covert channel. Finally, we discuss how other
        attacks benefit from knowing the complex addressng of a cache, such as
        sandboxed rowhammer.
  - Keywords: Complex addressing, Covert channel, Cross-Core, Last level cache,
        Reverse engineering, Side channel.
  - Source: https://www.s3.eurecom.fr/docs/raid15_maurice.pdf

- [THE MATHEMATICS OF THE PENTIUM DIVISION BUG](./pentiumbug.pdf)
  - Source: https://math.mit.edu/~edelman/homepage/papers/pentiumbug.pdf

- [A Survey of Algorithmic Methods in IC Reverse Engineering](./2021-1278.pdf)
  - Source: https://eprint.iacr.org/2021/1278.pdf

- [Laser-induced damage threshold of camera sensors and micro-optoelectromechanical systems](./034108_1.pdf)
  - Source: https://www.spiedigitallibrary.org/journalArticle/Download?urlId=10.1117%2F1.OE.56.3.034108&SSO=1

- [ReFace: Real-time Adversarial Attacks on Face Recognition Systems](./2206.04783.pdf)
  - Source: https://arxiv.org/pdf/2206.04783.pdf

- [Julia: A fresh approach to numerical computing](./julia_a_fresh.pdf)
  - Source: https://math.mit.edu/~edelman/publications/julia_a_fresh.pdf

- [Bit-Level Analysis of an SRT Divider Circuit](./dac96b.pdf)
  - Abstract: It is impractical to verify multiplier or di-
      vider circuits entirely at the bit-level using ordered Binary
      Decision Diagrams (BDDs), because the BDD representa-
      tions for these functions grow exponentially with the word
      size. It is possible, however, to analyze individual stages
      of these circuits using BDDs. Such analysis can be helpful
      when implementing complex arithmetic algorithms. As a
      demonstration,we showthat Intel could have used BDDs to
      detect erroneous lookup table entries in the Pentium(TM)
      floating point divider. Going beyond verification, we show
      that bit-level analysis can be used to generate a correct
      version of the table.
  - Source: https://www.cs.cmu.edu/~bryant/pubdir/dac96b.pdf

- [Anatomy of the Pentium Bug](./anapent.pdf)
  - Abstract: The Pentium computer chip’s division algorithm relies on a table from which five
      entries were inadvertently omitted, with the result that 1738 single precision dividend-
      divisor pairs yield relative errors whose most significant bit is uniformly distributed from
      the 14th to the 23rd (least significant) bit. This corresponds to a rate of one error every
      40 billion random single precision divisions. The same general pattern appears at double
      precision, with an error rate of one in every 9 billion divisions or 75 minutes of division
      time.
      These rates assume randomly distributed data. The distribution of the faulty pairs
      themselves however is far from random, with the effect that if the data is so nonrandom
      as to be just the constant 1, then random calculations started from that constant produce
      a division error once every few minutes, and these errors will sometimes propagate many
      more steps. A much higher rate yet is obtained when dividing small (< 100) integers
      “bruised” by subtracting one millionth, where every 400 divisions will see a relative error
      of at least one in a million.
      The software engineering implications of the bug include the observations that the
      method of exercising reachable components cannot detect reachable components mistak-
      enly believed unreachable, and that handchecked proofs build false confidence.
  - Source: http://boole.stanford.edu/pub/anapent.pdf 

- [Reverse Engineering x86 Processor Microcode](./sec17-koppe.pdf)
  - Abstract: Microcode is an abstraction layer on top of the phys-
      ical components of a CPU and present in most general-
      purpose CPUs today. In addition to facilitate complex and
      vast instruction sets, it also provides an update mechanism
      that allows CPUs to be patched in-place without requiring
      any special hardware. While it is well-known that CPUs
      are regularly updated with this mechanism, very little is
      known about its inner workings given that microcode and
      the update mechanism are proprietary and have not been
      throughly analyzed yet.
      In this paper, we reverse engineer the microcode seman-
      tics and inner workings of its update mechanism of con-
      ventional COTS CPUs on the example of AMD’s K8 and
      K10 microarchitectures. Furthermore, we demonstrate
      how to develop custom microcode updates. We describe
      the microcode semantics and additionally present a set of
      microprograms that demonstrate the possibilities offered
      by this technology. To this end, our microprograms range
      from CPU-assisted instrumentation to microcoded Tro-
      jans that can even be reached from within a web browser
      and enable remote code execution and cryptographic im-
      plementation attacks.
  - Source: https://www.usenix.org/system/files/conference/usenixsecurity17/sec17-koppe.pdf

- [Computational Aspects of the Pentium Affair](./pentium.pdf)
  - Source: https://people.cs.vt.edu/~naren/Courses/CS3414/assignments/pentium.pdf

- [FPnew: An Open-Source Multi-Format Floating-Point Unit Architecture for Energy-Proportional Transprecision Computing](./2007.01530.pdf)
  - Abstract: The slowdown of Moore’s law and the power wall
      necessitates a shift towards finely tunable precision (a.k.a. trans-
      precision) computing to reduce energy footprint. Hence, we need
      circuits capable of performing floating-point operations on a
      wide range of precisions with high energy-proportionality. We
      present FPnew, a highly configurable open-source transprecision
      floating-point unit (TP-FPU) capable of supporting a wide
      range of standard and custom FP formats. To demonstrate the
      flexibility and efficiency of FPnew in general-purpose processor
      architectures, we extend the RISC-V ISA with operations on
      half-precision, bfloat16, and an 8bit FP format, as well as SIMD
      vectors and multi-format operations. Integrated into a 32-bit
      RISC-V core, our TP-FPU can speed up execution of mixed-
      precision applications by 1.67x w.r.t. an FP32 baseline, while
      maintaining end-to-end precision and reducing system energy
      by 37%. We also integrate FPnew into a 64-bit RISC-V core,
      supporting five FP formats on scalars or 2, 4, or 8-way SIMD
      vectors. For this core, we measured the silicon manufactured in
      Globalfoundries 22FDX technology across a wide voltage range
      from 0.45V to 1.2V. The unit achieves leading-edge measured
      energy efficiencies between 178 Gflop/sW (on FP64) and 2.95
      Tflop/sW (on 8-bit mini-floats), and a performance between 3.2
      Gflop/s and 25.3 Gflop/s.
  - Keywords: Floating-Point Unit, RISC-V, Transprecision
      Computing, Multi-Format, Energy-Efficient.
  - Source: https://arxiv.org/pdf/2007.01530.pdf

- [ENERGY EFFICIENT FLOATING-POINT UNIT DESIGN](./1211_SamehGalal_FP_Design.pdf)
  - Source: https://vlsiwiki.stanford.edu/people/alum/pdf/1211_SamehGalal_FP_Design.pdf

- [OpenRAND: A Performance Portable, Reproducible Random Number Generation Library for Parallel Computations](./2310.19925.pdf)
  - Abstract: We introduce OpenRAND, a C++17 library aimed at facilitating reproducible scientific research through the generation of statis-
      tically robust and yet replicable random numbers. OpenRAND accommodates single and multi-threaded applications on CPUs
      and GPUs and offers a simplified, user-friendly API that complies with the C++ standard’s random number engine interface. It
      is portable: it functions seamlessly as a lightweight, header-only library, making it adaptable to a wide spectrum of software and
      hardware platforms. It is statistically robust: a suite of built-in tests ensures no pattern exists within single or multiple streams.
      Despite the simplicity and portability, it is remarkably performant—matching and sometimes even outperforming native libraries
      by a significant margin. Our tests, including a Brownian walk simulation, affirm its reproducibility and highlight its computational
      efficiency, outperforming CUDA’s cuRAND by up to 1.8 times
  - Source: https://arxiv.org/pdf/2310.19925.pdf

- [Parallel Random Numbers: As Easy as 1, 2, 3](./random123sc11.pdf)
  - Abstract: Most pseudorandom number generators (PRNGs) scale
      poorly to massively parallel high-performance computation
      because they are designed as sequentially dependent state
      transformations. We demonstrate that independent, keyed
      transformations of counters produce a large alternative
      class of PRNGs with excellent statistical properties (long
      period, no discernable structure or correlation).
      These
      counter-based PRNGs are ideally suited to modern multi-
      core CPUs, GPUs, clusters, and special-purpose hardware
      because they vectorize and parallelize well, and require little
      or no memory for state. We introduce several counter-based
      PRNGs:
      some based on cryptographic standards (AES,
      Threefish) and some completely new (Philox).
      All our
      PRNGs pass rigorous statistical tests (including TestU01’s
      BigCrush) and produce at least 264 unique parallel streams
      of random numbers, each with period 2128 or more.
      In
      addition to essentially unlimited parallel scalability, our
      PRNGs offer excellent single-chip performance: Philox is
      faster than the CURAND library on a single NVIDIA GPU.
  - Source: https://www.thesalmons.org/john/random123/papers/random123sc11.pdf

- [Squares: A Fast Counter-Based RNG](./2004.06278.pdf)
  - Abstract: In this article, we propose a new counter-based implementation of
      John von Neumann’s middle-square random number generator (RNG).
      Several rounds of squaring are applied to a counter to produce a ran-
      dom output. We discovered that four rounds are sufficient to provide
      satisfactory data. Two versions of the RNG are presented, a 4-round
      version with 32-bit output and a 5-round version with 64-bit output.
      Both pass stringent tests of randomness and may be the fastest counter-
      based generators.
  - Source: https://arxiv.org/pdf/2004.06278.pdf

- [Compiling without Continuations](./3062341.3062380.pdf)
  - Abstract: Many fields of study in compilers give rise to the concept
      of a join point—a place where different execution paths
      come together. Join points are often treated as functions
      or continuations, but we believe it is time to study them
      in their own right. We show that adding join points to a
      direct-style functional intermediate language is a simple
      but powerful change that allows new optimizations to be
      performed, including a significant improvement to list fusion.
      Finally, we report on recent work on adding join points to the
      intermediate language of the Glasgow Haskell Compiler.
  - Source: https://dl.acm.org/doi/pdf/10.1145/3062341.3062380

- [A correspondence between continuation passing style and static single assignment form](./202529.202532.pdf)
  - Source: https://dl.acm.org/doi/pdf/10.1145/202529.202532

- [Generalized Instruction Selection using SSA-Graphs](./2008-06-LCTES-ISelUsingSSAGraphs.pdf)
  - Abstract: Instruction selection is a well-studied compiler phase that trans-
      lates the compiler’s intermediate representation of programs to a
      sequence of target-dependent machine instructions optimizing for
      various compiler objectives (e.g. speed and space). Most existing
      instruction selection techniques are limited to the scope of a single
      statement or a basic block and cannot cope with irregular instruc-
      tion sets that are frequently found in embedded systems.
      We consider an optimal technique for instruction selection that
      uses Static Single Assignment (SSA) graphs as an intermediate
      representation of programs and employs the Partitioned Boolean
      Quadratic Problem (PBQP) for finding an optimal instruction se-
      lection. While existing approaches are limited to instruction pat-
      terns that can be expressed in a simple tree structure, we con-
      sider complex patterns producing multiple results at the same
      time including pre/post increment addressing modes, div-mod in-
      structions, and SIMD extensions frequently found in embedded
      systems. Although both instruction selection on SSA-graphs and
      PBQP are known to be NP-complete, the problem can be solved
      efficiently - even for very large instances.
      Our approach has been implemented in LLVM for an embedded
      ARMv5 architecture. Extensive experiments show speedups of up
      to 57% on typical DSP kernels and up to 10% on SPECINT 2000
      and MiBench benchmarks. All of the test programs could be com-
      piled within less than half a minute using a heuristic PBQP solver
      that solves 99.83% of all instances optimally.
  - Source: https://llvm.org/pubs/2008-06-LCTES-ISelUsingSSAGraphs.pdf

- [Near-Optimal Instruction Selection on DAGs](./2008-CGO-DagISel.pdf)
  - Abstract: Instruction selection is a key component of code generation. High
      quality instruction selection is of particular importance in the em-
      bedded space where complex instruction sets are common and code
      size is a prime concern. Although instruction selection on tree ex-
      pressions is a well understood and easily solved problem, instruc-
      tion selection on directed acyclic graphs is NP-complete. In this
      paper we present NOLTIS, a near-optimal, linear time instruction
      selection algorithm for DAG expressions. NOLTIS is easy to im-
      plement, fast, and effective with a demonstrated average code size
      improvement of 5.1% compared to the traditional tree decomposi-
      tion and tiling approach.
  - Source: https://llvm.org/pubs/2008-CGO-DagISel.pdf

- [Survey on Instruction Selection: An Extensive and Modern Literature Review](./1306.4898.pdf)
  - Abstract: Instruction selection is one of three optimization problems – the other two are instruction
      scheduling and register allocation – involved in code generation. The task of the instruction
      selector is to transform an input program from its target-independent representation into
      a target-specific form by making best use of the available machine instructions. Hence
      instruction selection is a crucial component of generating code that is both correct and
      runs efficiently on a specific target machine.
      Despite on-going research since the late 1960s, the last comprehensive survey on this
      field was written more than 30 years ago. As many new approaches and techniques have
      appeared since its publication, there is a need for an up-to-date review of the current
      body of literature; this report addresses that need by presenting an extensive survey and
      categorization of both dated method and the state-of-the-art of instruction selection. The
      report thereby supersedes and extends the previous surveys, and attempts to identify
      where future research could be directed.
  - Source: https://arxiv.org/ftp/arxiv/papers/1306/1306.4898.pdf

- [Physically Based Modeling and Animation of Fire](./fire.pdf)
  - Source: http://graphics.ucsd.edu/~henrik/papers/fire/fire.pdf

- [IAS15: A fast, adaptive, high-order integrator for gravitational dynamics, accurate to machine precision over a billion orbits](./1409.4779.pdf)
  - Abstract: We present IAS15, a 15th-order integrator to simulate gravitational dynamics. The integrator
  is based on a Gauß-Radau quadrature and can handle conservative as well as non-conservative
  forces. We develop a step-size control that can automatically choose an optimal timestep. The
  algorithm can handle close encounters and high-eccentricity orbits. The systematic errors are
  kept well below machine precision and long-term orbit integrations over 109 orbits show that
  IAS15 is optimal in the sense that it follows Brouwer’s law, i.e. the energy error behaves
  like a random walk. Our tests show that IAS15 is superior to a mixed-variable symplectic
  integrator (MVS) and other popular integrators, including high-order ones, in both speed and
  accuracy. In fact, IAS15 preserves the symplecticity of Hamiltonian systems better than the
  commonly-used nominally symplectic integrators to which we compared it.
  - Source: https://arxiv.org/pdf/1409.4779.pdf

- [The Synthesis of Complex Audio Spectra by Means of Frequency Modulation](./Chowning.pdf)
  - Source: https://people.ece.cornell.edu/land/courses/ece4760/Math/GCC644/FM_synth/Chowning.pdf

- [Alpha Blending with No Division Operations](./2202.02864.pdf)
  - Abstract: Highly accurate alpha blending can be performed entirely with integer
      operations, and no divisions. To reduce the number of integer multiplica-
      tions, multiple color components can be blended in parallel in the same
      32-bit or 64-bit register. This tutorial explains how to avoid division op-
      erations when alpha blending with 32-bit RGBA pixels. An RGBA pixel
      contains four 8-bit components (red, green, blue, and alpha) whose val-
      ues range from 0 to 255. Alpha blending requires multiplication of the
      color components by an alpha value, after which (for greatest accuracy)
      each of these products is divided by 255 and then rounded to the near-
      est integer. This tutorial presents an approximate alpha-blending formula
      that replaces the division operation with an integer shift and add—and
      also enables the number of multiplications to be reduced. When the same
      blending calculation is carried out to high precision using double-precision
      floating-point division operations, the results are found to exactly match
      those produced by this approximation. C++ code examples are included.
  - Source: https://arxiv.org/pdf/2202.02864.pdf

- [Depixelizing Pixel Art](./pixel.pdf)
  - Abstract: We describe a novel algorithm for extracting a resolution-
      independent vector representation from pixel art images, which en-
      ables magnifying the results by an arbitrary amount without im-
      age degradation. Our algorithm resolves pixel-scale features in the
      input and converts them into regions with smoothly varying shad-
      ing that are crisply separated by piecewise-smooth contour curves.
      In the original image, pixels are represented on a square pixel lat-
      tice, where diagonal neighbors are only connected through a single
      point. This causes thin features to become visually disconnected
      under magnification by conventional means, and creates ambigui-
      ties in the connectedness and separation of diagonal neighbors. The
      key to our algorithm is in resolving these ambiguities. This enables
      us to reshape the pixel cells so that neighboring pixels belonging
      to the same feature are connected through edges, thereby preserv-
      ing the feature connectivity under magnification. We reduce pixel
      aliasing artifacts and improve smoothness by fitting spline curves
      to contours in the image and optimizing their control points.
  - Source: https://johanneskopf.de/publications/pixelart/paper/pixel.pdf

- [Image Vectorization using Optimized Gradient Meshes](./imagevectorization_siggraph07.pdf)
  - Abstract: Recently, gradient meshes have been introduced as a powerful vec-
      tor graphics representation to draw multicolored mesh objects with
      smooth transitions. Using tools from Abode Illustrator and Corel
      CorelDraw, a user can manually create gradient meshes even for
      photo-realistic vector arts, which can be further edited, stylized and
      animated.
      In this paper, we present an easy-to-use interactive tool, called opti-
      mized gradient mesh, to semi-automatically and quickly create gra-
      dient meshes from a raster image. We obtain the optimized gradient
      mesh by formulating an energy minimization problem. The user
      can also interactively specify a few vector lines to guide the mesh
      generation. The resulting optimized gradient mesh is an editable
      and scalable mesh that otherwise would have taken many hours for
      a user to manually create.
  - Source: https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/imagevectorization_siggraph07.pdf

- [A Subdivision-Based Representation for Vector Image Editing](./vectorization.pdf)
  - Abstract: Vector graphics has been employed in a wide variety of applications due to its scalability and editability. Editability is a
      high priority for artists and designers who wish to produce vector-based graphical content with user interaction. In this paper, we
      introduce a new vector image representation based on piecewise smooth subdivision surfaces, which is a simple, unified and flexible
      framework that supports a variety of operations, including shape editing, color editing, image stylization, and vector image processing.
      These operations effectively create novel vector graphics by reusing and altering existing image vectorization results. Because image
      vectorization yields an abstraction of the original raster image, controlling the level of detail of this abstraction is highly desirable. To
      this end, we design a feature-oriented vector image pyramid that offers multiple levels of abstraction simultaneously. Our new vector
      image representation can be rasterized efficiently using GPU-accelerated subdivision. Experiments indicate that our vector image
      representation achieves high visual quality and better supports editing operations than existing representations.
  - Source: https://research.microsoft.com/en-us/um/people/hoppe/vectorization.pdf

- [Image vectorization and editing via linear gradient layer decomposition](./3592128.pdf)
  - Abstract: A key advantage of vector graphics over raster graphics is their editability.
      For example, linear gradients define a spatially varying color fill with a few
      intuitive parameters, which are ubiquitously supported in standard vector
      graphics formats and libraries. By layering regions filled with linear gradi-
      ents, complex appearances can be created. We propose an automatic method
      to convert a raster image into layered regions of linear gradients. Given an
      input raster image segmented into regions, our approach decomposes the
      resulting regions into opaque and semi-transparent linear gradient fills. Our
      approach is fully automatic (e.g., users do not identify a background as in
      previous approaches) and exhaustively considers all possible decompositions
      that satisfy perceptual cues. Experiments on a variety of images demonstrate
      that our method is robust and effective.
  - Source: https://dl.acm.org/doi/pdf/10.1145/3592128

- [Preventing Use-After-Free Attacks with Fast Forward Allocation](./ffmalloc_post_publication_revision.pdf)
  - Abstract: Memory-unsafe languages are widely used to implement crit-
      ical systems like kernels and browsers, leading to thousands
      of memory safety issues every year. A use-after-free bug is
      a temporal memory error where the program accidentally
      visits a freed memory location. Recent studies show that use-
      after-free is one of the most exploited memory vulnerabilities.
      Unfortunately, previous efforts to mitigate use-after-free bugs
      are not widely deployed in real-world programs due to either
      inadequate accuracy or high performance overhead.
      In this paper, we propose to resurrect the idea of one-time
      allocation (OTA) and provide a practical implementation with
      efficient execution and moderate memory overhead. With one-
      time allocation, the memory manager always returns a distinct
      memory address for each request. Since memory locations
      are not reused, attackers cannot reclaim freed objects, and
      thus cannot exploit use-after-free bugs. We utilize two tech-
      niques to render OTA practical: batch page management and
      the fusion of bump-pointer and fixed-size bins memory alloca-
      tion styles. Batch page management helps reduce the number
      of system calls which negatively impact performance, while
      blending the two allocation methods mitigates the memory
      overhead and fragmentation issues. We implemented a proto-
      type, called FFmalloc, to demonstrate our techniques. We eval-
      uated FFmalloc on widely used benchmarks and real-world
      large programs. FFmalloc successfully blocked all tested use-
      after-free attacks while introducing moderate overhead. The
      results show that OTA can be a strong and practical solution
      to thwart use-after-free threats.
  - Source: https://github.com/bwickman97/ffmalloc/raw/master/ffmalloc_post_publication_revision.pdf

- [HardsHeap: A Universal and Extensible Framework for Evaluating Secure Allocators](./10308833.pdf)
  - Source: https://par.nsf.gov/servlets/purl/10308833
  - Site: https://github.com/kaist-hacking/HardsHeap

- [Automatic Techniques to Systematically Discover New Heap Exploitation Primitives](./sec20fall_yun_prepub.pdf)
  - Source: https://www.usenix.org/system/files/sec20fall_yun_prepub.pdf
  - Site: https://github.com/sslab-gatech/ArcHeap

- [Mimalloc: Free List Sharding in Action](./mimalloc-tr-v1.pdf)
  - Source: https://www.microsoft.com/en-us/research/uploads/prod/2019/06/mimalloc-tr-v1.pdf
  - Site: https://github.com/microsoft/mimalloc/